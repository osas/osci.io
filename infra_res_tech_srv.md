---
title: Underlying Technical Services
---

| Service        | Location                                             | Description                                                             | Status |
|:--------------:|:----------------------------------------------------:|:-----------------------------------------------------------------------:|:------:|
| MX1            | polly.osci.io                                        | Mail redirections, RT mail accounts                                     | OK     |
| MX2            | polly.osci.io<br/>francine.osci.io<br/>carla.osci.io | backup mail server for OSCI and communities                             | OK     |
| NS1            | polly.osci.io                                        | DNS master server                                                       | OK     |
| NS2            | polly.osci.io<br/>francine.osci.io<br/>carla.osci.io | DNS slave server for OSCI and communities                               | OK     |
| NTP            | ntp1.osci.io<br/>ntp2.osci.io                        | NTP stratum 2 server using a CDMA device                                | OK     |
| PXE            | speedy.osci.io<br/>pxe.osci.io                       | DHCP/TFTP, netboot menu for various distros, manual/unattended installs | OK     |
| Ticket Tracker | tickets.osci.io                                      | RT for OSCI and communities (RDO Cloud, Fedora Infra Security)          | OK     |
| VMs            | speedy.osci.io<br/>guido.osci.io<br/>jerry.osci.io   | hypervisor                                                              | OK     |
| VMs            | seymour.osci.io                                      | hypervisor                                                              | HOLD   |
| VMs            | crow-[1-3].osci.io                                   | oVirt and OpenShift experimentation                                     | WIP    |
| Storage        | lucille.srv.osci.io                                  | NFS server providing extra remote storage for VMs or blades             | OK     |
| Supervision    | catton.osci.io                                       | Monitoring, alerting                                                    | OK     |

