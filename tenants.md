---
title: Tenants
---

## Embedded Communities

Embedded communities are tenants with whom we work closely on their infrastructure.

| Tenant                      | Involvement                                                       |
|:---------------------------:|:-----------------------------------------------------------------:|
| Beaker                      | member of the infrastructure team                                 |
| Gluster                     | lead of the infrastructure team                                   |
| RDO                         | member of the infrastructure team                                 |
| oVirt                       | member of the infrastructure team                                 |
| Project Atomic / Silverblue | lead of the infrastructure team<br/>working with Fedora sysadmins |
| Pulp                        | member of the infrastructure team                                 |
| Software Collections        | member of the infrastructure team                                 |
| Theopensourceway.org        | infrastructure team                                               |
 

## Tenants of the Community Cage

The following tenants are hosted in the Community Cage (co-location services for various community projects):

* CentOS
* Ceph
* Fedora
* Gluster
* GNOME
* Trystack

## Hosted Tenants

We provide VMs, containers, and services to Hosted Tenants using the OSCI infrastructure.

|                     |                   |                                          |                                                                   |  Level of     |        |
| Tenant              | Service           | Location                                 | Description                                                       | Involvement   | Status |
|:-------------------:|:-----------------:|:----------------------------------------:|:-----------------------------------------------------------------:|:-------------:|:------:|
| Beaker              | Website / Mails   | www.beaker-project.org<br/>mail.beaker-project.org | Webserver with rsync uploads for website and RPMs, mail aliases   | collaboration | OK     |
| Community           | Website Builder   | community-web-builder.int.osci.io        | Middleman builder for community.redhat.com                        | full          | OK     |
|                     | Website           | community.redhat.com                     | Webserver for community.redhat.com                                | full          | OK     |
| Fedora              | Translation Tool  | fedora.zanata.org                        | Zanata instance                                                   | full          | WIP    |
| Fedora Atomic       | Test nodes        | fedora-atomic-[1-6].osci.io              | dedicated blades for tests                                        | collaboration | OK     |
| GDB                 | Build machine     | gdb-buildbot.osci.io                     | Buildbot                                                          | hosting-only  | OK     |
| Gnocchi             | Website           | gnocchi.osci.io                          | Gnocchi & client documentation builder and web server, web server | full          | OK     |
| Heptapod            | CI                | heptapod.osci.io                         | dedicated blade for CI node                                       | hosting-only  | OK     |
| JBOSS               | MLs               | lists.jboss.org                          | Mailman 3                                                         | full          | WIP    |
| Kiali               | OpenShift Service Mesh bot | kiali-bot.osci.io               |                                                                   | hosting-only  | OK     |
| Minishift           | MLs               | lists.minishift.io                       | Mailman 3                                                         | full          | OK     |
| NFS Ganesha         | MLs               | lists.nfs-ganesha.org                    | Mailman 3                                                         | full          | OK     |
|                     | Web redirection   | www.nfs-ganesha.org                      | redirection to GitHub wiki                                        | full          | OK     |
|                     | File zone         | download.nfs-ganesha.org                 | Webserver with SFTP uploads                                       | full          | OK     |
| Open Data Hub       | MLs               | lists.opendatahub.io                     | Mailman 3                                                         | full          | OK     |
| Open Source Infra   | MLs               | lists.opensourceinfra.org                | Mailman 3                                                         | full          | OK     |
| OpenJDK             | Sources           | openjdk-sources.osci.io                  | Webserver and SFTP accounts                                       | full          | OK     |
| OSPO/OSCI           | Analytics         | cauldron.osci.io                         | Cauldron instance                                                 | hosting-only  | OK     |
| oVirt               | MLs               | mail.ovirt.org                           | Mailman 3, web UI on lists.ovirt.org                              | collaboration | OK     |
|                     | Website Builder   | ovirt-web-builder.int.osci.io            | Middleman builder for www.ovirt.org                               | collaboration | OK     |
|                     | Website           | www.ovirt.org                            | Webserver for www.ovirt.org                                       | collaboration | OK     |
|                     | Monitoring        | monitoring.ovirt.org                     | Icinga                                                            | collaboration | OK     |
|                     | Glance            | glance.ovirt.org                         | Glance instance                                                   | collaboration | OK     |
| Patternfly          | Forum             | patternfly-forum.osci.io                 | Discourse                                                         | collaboration | OK     |
| PCP                 | Tools             |                                          | Taskboard and various tools                                       | hosting-only  | OK     |
| PO4A                | Website           | www.po4a.org                             | Jekyll or ASCII Binder builder and webserver                      | full          | OK     |
|                     | MLs               | lists.po4a.org                           | Mailman 3                                                         | full          | OK     |
| Podman              | MLs               | lists.podman.io<br/>lists.buildah.io     | Mailman 3, shared with the Buildah project                        | full          | OK     |
| Pulp                | Website Builder   | pulp-web-builder.int.osci.io             | Jekyll builder for pulpproject.org                                | collaboration | OK     |
|                     | Websites          | pulpproject.org<br/>docs.pulpproject.org | Webserver for project pages and documentation                     | collaboration | OK     |
| Python              | Builder           | python-builder-rawhide.osci.io           | CI Builder                                                        | hosting-only  | OK     |
|                     | Builder           | python-builder2-rawhide.osci.io          | CI Builder                                                        | hosting-only  | OK     |
|                     | Builder           | python-builder-rhel7.osci.io             | CI Builder                                                        | hosting-only  | OK     |
|                     | Builder           | python-builder-rhel8.osci.io             | CI Builder                                                        | hosting-only  | OK     |
|                     | Builder           | python-builder-rhel8-fips.osci.io        | CI Builder                                                        | hosting-only  | OK     |
| RDO                 | Website Builder   | rdo-web-builder.int.osci.io              | Middleman builder for www.rdoproject.org                          | collaboration | OK     |
| SCL                 | Website           | www.softwarecollections.org              | Webserver                                                         | hosting-only  | OK     |
| Software Heritage   | Computation       | Indexing/Processing node                 | dedicated blade                                                   | hosting-only  | WIP    |
| Spice               | Website           | www.spice-space.org                      | Webserver                                                         | full          | OK     |
| The Open Source Way | Website           | theopensourceway.org                     | Mediawiki                                                         | full          | OK     |
| Zanata              | Translation Tool  | translate.zanata.org                     | Zanata instance                                                   | full          | OK     |


## Externaly Hosted Tenants

We also provide services using third-party plateforms to complement our own infrastructure.

| Tenant         | Service        | Location                        | Third-Party      | Status |
|:--------------:|:--------------:|:-------------------------------:|:----------------:|:------:|
| Gluster        | Website + Blog | www.gluster.org                 | WPEngine         | OK     |
| Fedora         | Blog           | communityblog.fedoraproject.org | WPEngine         | OK     |
|                | Blog           | fedoramagazine.org              | WPEngine         | OK     |
| oVirt          | Blog           | blogs.ovirt.org                 | WPEngine         | OK     |
| OSPO           | Website        | next.redhat.com                 | WPEngine         | OK     |
| Project Atomic | Website        | www.projectatomic.io            | OpenShift Online | OK     |
| RDO            | Blog           | blogs.rdoproject.org            | WPEngine         | OK     |

